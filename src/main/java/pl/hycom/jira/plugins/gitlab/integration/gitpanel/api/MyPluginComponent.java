package pl.hycom.jira.plugins.gitlab.integration.gitpanel.api;

public interface MyPluginComponent
{
    String getName();
}